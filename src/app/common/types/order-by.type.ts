export interface IOrderBy {
  type: string;
  field: string;
}
