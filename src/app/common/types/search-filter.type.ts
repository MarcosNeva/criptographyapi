export interface ISearchFilter {
  text: string;
  fields: string[];
}
