import { DatabaseService } from '../../../database/database';
import { Service } from 'typedi';

import { SearchFilter } from '../../common/search-filter';
import { SearchFilterService } from '../../common/search-filter.service';
import { User } from '../user.model';

@Service()
export class UserRepository
{

  constructor(
    private readonly databaseService: DatabaseService,
    private readonly searchFilterService: SearchFilterService
  ) {}

  
//Repositorio similar a companies, pero ahora necesitamos usuarios en la BBDD, necesitamos el método create, findAll, findByName y findUser


  // Permite crear usuarios, se insertan con sql en la tabla user los datos proporcionados
  async create(
     user_Id, name, surname, email, password
  ): Promise<User> {
    const queryDoc = {
      sql: 'INSERT INTO core.user (id, name, surname, email, password) VALUES ($1, $2, $3, $4, $5) RETURNING *',
      params: [ user_Id, name, surname, email, password ]
    };

    const user = await this.databaseService.execQuery(queryDoc);
    return user.rows[0];
  }

  
  // Se genera una lista de usuarios, incluyendo todos los campos que tienen
  async findAll(searchFilter?: SearchFilter): Promise<User[]> {
    const queryDoc: any = {
      sql: 'SELECT * FROM core.user'
    };

    if (searchFilter != null) {
      const whereDoc = this.searchFilterService.transformSearchFilterToSQL(searchFilter);

      if (searchFilter.search != null || searchFilter.filter != null) {
        queryDoc.sql = queryDoc.sql + ` WHERE `;
      }

      queryDoc.sql = queryDoc.sql + whereDoc.sql;
      queryDoc.params = whereDoc.params;
    }

    const users = await this.databaseService.execQuery(queryDoc);
    return users.rows;
  }


  //Nos permite identificar un usuario con un nombre específico
  async findByName(name: string): Promise<User | null> {
    const queryDoc = {
      sql: 'SELECT * FROM core.user WHERE name = $1',
      params: [name]
    };

    const users = await this.databaseService.execQuery(queryDoc);

    return users.rows[0] || null;
  }


  //Nos permite identificar un usuario con un mail y contraseña específico
  async findUser(email: string, password: string): Promise<User | null> {
    const queryDoc = {
      sql: 'SELECT * FROM core.user WHERE email = $1 AND password = $2',
      params: [email, password]
    };

    const users = await this.databaseService.execQuery(queryDoc);

    return users.rows[0] || null;
  }


}
