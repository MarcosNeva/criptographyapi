export const development: any = {

  dbOptions:
  {
    user: 'postgres',
    host: '127.0.0.1',
    database: 'postgres',
    password: 'hola1234',
    port: 5432
  },
  
  //Valor de session : Clave de firma, cuanto mas larga sea mejor. Ahora el valor es inseguro ya que va en un fichero de configuracion

  secrets:
  {
    session: 'hhsdfbdshfbf_dfhsdfbsdf.11SSSaadadif',
    expiresIn: 60 * 60 * 24 * 30                        //Tiempo en el que expirará la clave = 1 mes
  }
  
};