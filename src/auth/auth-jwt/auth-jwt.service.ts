import { Service } from "typedi";
import { User } from '../../app/user/user.model';
import { config } from '../../config/environment';
import * as jwt from 'jsonwebtoken';

const JWT_SECRET = config.secrets.session;

@Service()
export class AuthJwtService {

    createToken(user: User): string 
    { 
        const jwtOptions = { expiresIn: config.secrets.expiresIn };         //Login con id y mail una vez tenemos el token adecuado
        const { id, email } = user;
        
        return jwt.sign({userId: id, email}, JWT_SECRET, jwtOptions);
    }

    verifyToken(token: string) {                                            //Middleware verifica token
        return jwt.verify(token, JWT_SECRET);
    }
}