import { StatusCodes } from 'http-status-codes';
import { isEmpty, isNil } from 'lodash';
import { Service } from "typedi";
import { AuthCredentialsService } from './auth-credentials.service';

@Service()
export class AuthCredentialsController
{
    
    constructor(private readonly authCredentialsService: AuthCredentialsService) {}
    
    //Funcion Login
    async login(req, res, next): Promise<void>
    {
        if(isNil(req.body) || isEmpty(req.body) || isNil(req.body.email) || isNil(req.body.password)){                  //Los campos no pueden ser nulos ni estar vacíos
            return next(res.sendStatus(StatusCodes.BAD_REQUEST));
        }
        try {
            const token = await this.authCredentialsService.login(req.body);
            res.send(token);
        }
        catch (err) {
            res.sendStatus(StatusCodes.UNAUTHORIZED);
        }
    }


    //Funcion Register
    async register(req, res, next): Promise<void>
    {                                                                                                                   //Los campos no pueden ser nulos ni estar vacíos     
        if(isNil(req.body) || isEmpty(req.body) || isNil(req.body.id) || isEmpty(req.body.id) || isNil(req.body.name) || isEmpty(req.body.name) || isNil(req.body.surname) || isEmpty(req.body.surname) || isNil(req.body.email) || isEmpty(req.body.email) || isNil(req.body.password) || isEmpty(req.body.password)) {
            return next(res.sendStatus(StatusCodes.BAD_REQUEST));
        }
        try{
            const token = await this.authCredentialsService.register(req.body);
            res.send(token);
        }
        catch (err) {
            res.sendStatus(StatusCodes.UNAUTHORIZED);
        }
    }
}