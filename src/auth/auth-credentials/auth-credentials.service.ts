import { Service } from "typedi";
import { User } from '../../app/user/user.model';
import { AuthJwtService } from '../auth-jwt/auth-jwt.service';
import { UserRepository } from '../../app/user/repository/user.repository'


@Service()
export class AuthCredentialsService
{
    constructor(private jwtService: AuthJwtService, private userRepository: UserRepository) {}

    // Funcion que permite logear usuarios ya existentes en la BBDD. Si el email y la password son correctos se generará un token para firmar peticiones posteriormente
    async login({email, password}): Promise<any>
    { 
        if(await this.userRepository.findUser(email, password) != null)
        {
            //En full object recogeremos todos los datos de la columna de la BBDD, solo nos interesa recuperar los campos emial y password, que tendra la estructura de User.
            var fullObject: any;
            var valuesUser: User;
            
            fullObject = await this.userRepository.findUser(email, password);
            valuesUser = <User> fullObject;

            
            return this.createUserToken(valuesUser);            //Se genera el token de usuario
        }
        else
        {
            throw new Error('Invalid email or password');       //Error si el email o la password es incorrecta, no existe en BBDD
        }
    }


    // Funcion que permita registrar usuarios en la BBDD. 
    async register({id, name, surname, email, password}): Promise<any>
    {
        if(id != null && name != null && surname != null && email != null && password != null)    //No tiene que haber ningun campo vacío para efectuar el registro correctamente
        {
            //En full object recogeremos todos los datos de la columna de la BBDD, solo nos interesa recuperar los campos emial y password, que tendra la estructura de User.
            var fullObject: any;
            var valuesUser: User;
            
            fullObject = await this.userRepository.findByName(name);
            valuesUser = <User> fullObject;

            if(valuesUser == null )                                     // Comprobacion de errores, no tiene que haber nombres repetidos en la BBDD
            {
                this.userRepository.create(id, name, surname, email, password);
                return "The registration has been completed";
            }
            else
            {
                return "The username that you wrote is already token";
            } 
        }
        else
        {
            return "Please, in order to register you must fill all the fields";
        }     
    }

    private createUserToken(user: User)
    {
        //Objeto con la informacion que queremos devolver del login, id y mail
        const{
            id, email //Se desestructura id y mail del objeto user
        } = user;

        return {
            user: {
                id, email
            },
            //Objeto con propiedad token que firmaremos en createToken
            token: this.jwtService.createToken(user) //Servicio logica de JSON web tokens, metodo especifico por si necesito crear tokens en otro lugar del código
        }
    }
}