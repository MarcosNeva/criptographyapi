export interface AuthMiddleware {
    validateRequest(req, res, next): void;
}