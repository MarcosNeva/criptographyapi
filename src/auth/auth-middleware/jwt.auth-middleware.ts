import Container, { Service } from 'typedi';
import { AuthMiddleware } from './auth-middleware';
import { StatusCodes } from 'http-status-codes';
import { AuthJwtService } from '../auth-jwt/auth-jwt.service';
import { ExtractJwt } from 'passport-jwt';

@Service()
export class JwtMiddleware implements AuthMiddleware {

    validateRequest(req, res, next): Promise<void> {
        const jwtService = Container.get(AuthJwtService);
        try 
        {
            //Extrae el jwt del header Authorization
            const token = ExtractJwt.fromAuthHeaderAsBearerToken()(req);
            if (token != null) 
            {
                //Decodificamos el token y lo verificamos
                const decodeToken = jwtService.verifyToken(token);
                req.user = decodeToken;

                //Autorizar o denegar
                return next();
            }

            //Autorizar o denegar
            return next(res.sendStatus(StatusCodes.UNAUTHORIZED)); //Error 400
        }
        catch (err) 
        {
            //Autorizar o denegar
            return next(res.sendStatus(StatusCodes.UNAUTHORIZED)); //Error 400
        }
    }
}