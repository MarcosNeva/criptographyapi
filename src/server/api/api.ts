import { Router } from 'express';
import { Service } from 'typedi';
import { createJSDocThisTag } from 'typescript';
import { CompanyController } from '../../app/companies/company.controller';
import { CompanyTypeController } from '../../app/company-types/company-type.controller';
import { AuthCredentialsController } from '../../auth/auth-credentials/auth-credentials.controller';

@Service()
export class Api {
  private apiRouter: Router;  //Router para todas las peticiones del API
  private authRouter: Router; //Router para peticiones firmadas

  constructor(private companyController: CompanyController,
              private companyTypeController: CompanyTypeController,
              private authCredentialsController: AuthCredentialsController
  ) {
    this.initRouterAndSetApiRoutes();
    this.initRouterAndSetAuthRoutes();
  }

  getApiRouter(): Router {
    return this.apiRouter; 
  }

  getAuthRouter(): Router {
    return this.authRouter;
  }

  private initRouterAndSetApiRoutes(): void {
    this.apiRouter = Router();

    //Company types endpoints
    this.apiRouter.get(
      '/company_types',
      (req, res, next) => this.companyTypeController.getAll(req, res, next)
    );


    //Companies endpoints
    this.apiRouter.post(
      '/companies',
      (req, res, next) => this.companyController.create(req, res, next)
    );

    this.apiRouter.get(
      '/companies',
      (req, res, next) => this.companyController.getAll(req, res, next)
    );

    this.apiRouter.get(
      '/companies/:id',
      (req, res, next) => this.companyController.getById(req, res, next)
    );

    this.apiRouter.put(
      '/companies/:id',
      (req, res, next) => this.companyController.update(req, res, next)
    );

    this.apiRouter.delete(
      '/companies/:id',
      (req, res, next) => this.companyController.delete(req, res, next)
    );

  }

  private initRouterAndSetAuthRoutes(): void {
    this.authRouter = Router();

    //Login endpoints
    this.authRouter.post(
      '/', (req, res, next) => this.authCredentialsController.login(req, res, next) 
    )

    //Register endpoints
    this.authRouter.post(
      '/register', (req, res, next) => this.authCredentialsController.register(req, res, next) 
    )
  }

}
