import { json, urlencoded } from 'body-parser';
import { Application } from 'express';
import { Service } from 'typedi';

import * as cors from 'cors';
import * as express from 'express';
import * as http from 'http';
import * as morgan from 'morgan';

import { Api } from './api/api';
import { config } from '../config/environment';
import { JwtMiddleware } from '../auth/auth-middleware/jwt.auth-middleware';

@Service()
export class Server {

  app: Application;
  httpServer: http.Server;

  constructor(private readonly api: Api, private readonly jwtMiddleware:JwtMiddleware) {
    this.app = express();
    this.setupServer();
  }

  private setupServer(): void {
    this.app.use(cors());
    this.app.use(json({ limit: '5mb' }));
    this.app.use(urlencoded({ extended: false }));
    this.app.use(morgan('dev'));

    this.app.use('/auth', this.api.getAuthRouter());          //Router diferente para que el jwtMiddleware no afecte a todos los endpoints de /auth. Si no, necesitaríamos un token para realizar un login y sería imposible
    

    this.app.use('/api', this.jwtMiddleware.validateRequest); //Comprobar el token que nos permitirá firmar las consultas
    this.app.use('/api', this.api.getApiRouter());            //Realizar la consulta pedida

    this.httpServer = this.app.listen(config.port, this.onHttpServerListening);
  }

  private onHttpServerListening(): void {
    console.log('Server Express started in %s mode (ip: %s, port: %s)', config.env, config.ip, config.port);
  }

}
