# **API test for Criptography Work**


This repository includes a simple API to be able to explain the operation of the JWS, and the importance of security in an API.

Important to install before launching on terminal(make sure you have node js installed on windows): 

	yarn 
	npm install -g nodemon   
	npm install -g typescript
	npm install -g ts-node  

To run:
	yarn build
	yarn dev 

