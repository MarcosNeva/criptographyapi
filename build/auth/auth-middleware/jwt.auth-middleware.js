"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtMiddleware = void 0;
const tslib_1 = require("tslib");
const typedi_1 = require("typedi");
const http_status_codes_1 = require("http-status-codes");
const auth_jwt_service_1 = require("../auth-jwt/auth-jwt.service");
const passport_jwt_1 = require("passport-jwt");
let JwtMiddleware = class JwtMiddleware {
    validateRequest(req, res, next) {
        const jwtService = typedi_1.default.get(auth_jwt_service_1.AuthJwtService);
        try {
            //Extrae el jwt del header Authorization
            const token = passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken()(req);
            if (token != null) {
                //Decodificamos el token y lo verificamos
                const decodeToken = jwtService.verifyToken(token);
                req.user = decodeToken;
                //Autorizar o denegar
                return next();
            }
            //Autorizar o denegar
            return next(res.sendStatus(http_status_codes_1.StatusCodes.UNAUTHORIZED)); //Error 400
        }
        catch (err) {
            //Autorizar o denegar
            return next(res.sendStatus(http_status_codes_1.StatusCodes.UNAUTHORIZED)); //Error 400
        }
    }
};
JwtMiddleware = tslib_1.__decorate([
    typedi_1.Service()
], JwtMiddleware);
exports.JwtMiddleware = JwtMiddleware;
//# sourceMappingURL=jwt.auth-middleware.js.map