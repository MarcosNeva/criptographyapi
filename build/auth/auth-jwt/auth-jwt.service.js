"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthJwtService = void 0;
const tslib_1 = require("tslib");
const typedi_1 = require("typedi");
const environment_1 = require("../../config/environment");
const jwt = require("jsonwebtoken");
const JWT_SECRET = environment_1.config.secrets.session;
let AuthJwtService = class AuthJwtService {
    createToken(user) {
        const jwtOptions = { expiresIn: environment_1.config.secrets.expiresIn }; //Login con id y mail una vez tenemos el token adecuado
        const { id, email } = user;
        return jwt.sign({ userId: id, email }, JWT_SECRET, jwtOptions);
    }
    verifyToken(token) {
        return jwt.verify(token, JWT_SECRET);
    }
};
AuthJwtService = tslib_1.__decorate([
    typedi_1.Service()
], AuthJwtService);
exports.AuthJwtService = AuthJwtService;
//# sourceMappingURL=auth-jwt.service.js.map