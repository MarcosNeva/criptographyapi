"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthCredentialsController = void 0;
const tslib_1 = require("tslib");
const http_status_codes_1 = require("http-status-codes");
const lodash_1 = require("lodash");
const typedi_1 = require("typedi");
const auth_credentials_service_1 = require("./auth-credentials.service");
let AuthCredentialsController = class AuthCredentialsController {
    constructor(authCredentialsService) {
        this.authCredentialsService = authCredentialsService;
    }
    //Funcion Login
    login(req, res, next) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (lodash_1.isNil(req.body) || lodash_1.isEmpty(req.body) || lodash_1.isNil(req.body.email) || lodash_1.isNil(req.body.password)) { //Los campos no pueden ser nulos ni estar vacíos
                return next(res.sendStatus(http_status_codes_1.StatusCodes.BAD_REQUEST));
            }
            try {
                const token = yield this.authCredentialsService.login(req.body);
                res.send(token);
            }
            catch (err) {
                res.sendStatus(http_status_codes_1.StatusCodes.UNAUTHORIZED);
            }
        });
    }
    //Funcion Register
    register(req, res, next) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (lodash_1.isNil(req.body) || lodash_1.isEmpty(req.body) || lodash_1.isNil(req.body.id) || lodash_1.isEmpty(req.body.id) || lodash_1.isNil(req.body.name) || lodash_1.isEmpty(req.body.name) || lodash_1.isNil(req.body.surname) || lodash_1.isEmpty(req.body.surname) || lodash_1.isNil(req.body.email) || lodash_1.isEmpty(req.body.email) || lodash_1.isNil(req.body.password) || lodash_1.isEmpty(req.body.password)) {
                return next(res.sendStatus(http_status_codes_1.StatusCodes.BAD_REQUEST));
            }
            try {
                const token = yield this.authCredentialsService.register(req.body);
                res.send(token);
            }
            catch (err) {
                res.sendStatus(http_status_codes_1.StatusCodes.UNAUTHORIZED);
            }
        });
    }
};
AuthCredentialsController = tslib_1.__decorate([
    typedi_1.Service(),
    tslib_1.__metadata("design:paramtypes", [auth_credentials_service_1.AuthCredentialsService])
], AuthCredentialsController);
exports.AuthCredentialsController = AuthCredentialsController;
//# sourceMappingURL=auth-credentials.controller.js.map