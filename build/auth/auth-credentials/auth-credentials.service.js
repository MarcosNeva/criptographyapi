"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthCredentialsService = void 0;
const tslib_1 = require("tslib");
const typedi_1 = require("typedi");
const auth_jwt_service_1 = require("../auth-jwt/auth-jwt.service");
const user_repository_1 = require("../../app/user/repository/user.repository");
let AuthCredentialsService = class AuthCredentialsService {
    constructor(jwtService, userRepository) {
        this.jwtService = jwtService;
        this.userRepository = userRepository;
    }
    // Funcion que permite logear usuarios ya existentes en la BBDD. Si el email y la password son correctos se generará un token para firmar peticiones posteriormente
    login({ email, password }) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if ((yield this.userRepository.findUser(email, password)) != null) {
                //En full object recogeremos todos los datos de la columna de la BBDD, solo nos interesa recuperar los campos emial y password, que tendra la estructura de User.
                var fullObject;
                var valuesUser;
                fullObject = yield this.userRepository.findUser(email, password);
                valuesUser = fullObject;
                return this.createUserToken(valuesUser); //Se genera el token de usuario
            }
            else {
                throw new Error('Invalid email or password'); //Error si el email o la password es incorrecta, no existe en BBDD
            }
        });
    }
    // Funcion que permita registrar usuarios en la BBDD. 
    register({ id, name, surname, email, password }) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (id != null && name != null && surname != null && email != null && password != null) //No tiene que haber ningun campo vacío para efectuar el registro correctamente
             {
                //En full object recogeremos todos los datos de la columna de la BBDD, solo nos interesa recuperar los campos emial y password, que tendra la estructura de User.
                var fullObject;
                var valuesUser;
                fullObject = yield this.userRepository.findByName(name);
                valuesUser = fullObject;
                if (valuesUser == null) // Comprobacion de errores, no tiene que haber nombres repetidos en la BBDD
                 {
                    this.userRepository.create(id, name, surname, email, password);
                    return "The registration has been completed";
                }
                else {
                    return "The username that you wrote is already token";
                }
            }
            else {
                return "Please, in order to register you must fill all the fields";
            }
        });
    }
    createUserToken(user) {
        //Objeto con la informacion que queremos devolver del login, id y mail
        const { id, email //Se desestructura id y mail del objeto user
         } = user;
        return {
            user: {
                id, email
            },
            //Objeto con propiedad token que firmaremos en createToken
            token: this.jwtService.createToken(user) //Servicio logica de JSON web tokens, metodo especifico por si necesito crear tokens en otro lugar del código
        };
    }
};
AuthCredentialsService = tslib_1.__decorate([
    typedi_1.Service(),
    tslib_1.__metadata("design:paramtypes", [auth_jwt_service_1.AuthJwtService, user_repository_1.UserRepository])
], AuthCredentialsService);
exports.AuthCredentialsService = AuthCredentialsService;
//# sourceMappingURL=auth-credentials.service.js.map