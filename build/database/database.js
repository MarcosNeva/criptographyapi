"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseService = void 0;
const tslib_1 = require("tslib");
const pg_1 = require("pg");
const db_transaction_1 = require("./models/db-transaction");
const environment_1 = require("../config/environment");
class DatabaseService {
    /**
     * Función que conecta con la base de datos e inicializa el pool de conexiones
     */
    initConnectionPool() {
        this.dbPool = this.createPostgreSQLInstance(environment_1.config.dbOptions);
    }
    /**
     * Esta función tiene el objetivo de ejecutar una determinada consulta SQL y
     * devolver los resultados de la ejecución.
     */
    execQuery(query) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const dbClient = yield this.getConnection();
            const { sql, params } = query;
            try {
                const { rows, rowCount } = yield dbClient.query({ text: sql, values: params });
                return { rows, rowCount };
            }
            finally {
                dbClient.release();
            }
        });
    }
    /**
     * Esta función tiene el objetivo de crear una nueva conexión con el pool e
     * inicializar la transacción, no cerramos la conexión del pool ya que esta
     * tiene que estar activa hasta que se haga el COMMIT o el ROLLBACK
     * correspondiente.
     */
    startTransaction() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const dbClient = yield this.getConnection();
            return new db_transaction_1.DBTransaction(dbClient);
        });
    }
    /**
     * Si hay una instancia creada la devolvemos, sino creamos un nuevo pool de
     * conexiones.
     */
    createPostgreSQLInstance(dbOptions) {
        if (this.dbPool != null) {
            return this.dbPool;
        }
        return new pg_1.Pool({
            user: dbOptions.user,
            host: dbOptions.host,
            database: dbOptions.database,
            password: dbOptions.password,
            port: dbOptions.port,
        });
    }
    /**
     * Esta función permite devolver el cliente resultante de conectarse al pool.
     */
    getConnection() {
        return this.dbPool.connect();
    }
}
exports.DatabaseService = DatabaseService;
//# sourceMappingURL=database.js.map