"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
require("reflect-metadata");
const database_1 = require("./database/database");
const typedi_1 = require("typedi");
const server_1 = require("./server/server");
init();
function init() {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const containterDB = typedi_1.Container.get(database_1.DatabaseService);
        containterDB.initConnectionPool();
        typedi_1.Container.get(server_1.Server);
    });
}
//# sourceMappingURL=index.js.map