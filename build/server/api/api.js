"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Api = void 0;
const tslib_1 = require("tslib");
const express_1 = require("express");
const typedi_1 = require("typedi");
const company_controller_1 = require("../../app/companies/company.controller");
const company_type_controller_1 = require("../../app/company-types/company-type.controller");
const auth_credentials_controller_1 = require("../../auth/auth-credentials/auth-credentials.controller");
let Api = class Api {
    constructor(companyController, companyTypeController, authCredentialsController) {
        this.companyController = companyController;
        this.companyTypeController = companyTypeController;
        this.authCredentialsController = authCredentialsController;
        this.initRouterAndSetApiRoutes();
        this.initRouterAndSetAuthRoutes();
    }
    getApiRouter() {
        return this.apiRouter;
    }
    getAuthRouter() {
        return this.authRouter;
    }
    initRouterAndSetApiRoutes() {
        this.apiRouter = express_1.Router();
        //Company types endpoints
        this.apiRouter.get('/company_types', (req, res, next) => this.companyTypeController.getAll(req, res, next));
        //Companies endpoints
        this.apiRouter.post('/companies', (req, res, next) => this.companyController.create(req, res, next));
        this.apiRouter.get('/companies', (req, res, next) => this.companyController.getAll(req, res, next));
        this.apiRouter.get('/companies/:id', (req, res, next) => this.companyController.getById(req, res, next));
        this.apiRouter.put('/companies/:id', (req, res, next) => this.companyController.update(req, res, next));
        this.apiRouter.delete('/companies/:id', (req, res, next) => this.companyController.delete(req, res, next));
    }
    initRouterAndSetAuthRoutes() {
        this.authRouter = express_1.Router();
        //Login endpoints
        this.authRouter.post('/', (req, res, next) => this.authCredentialsController.login(req, res, next));
        //Register endpoints
        this.authRouter.post('/register', (req, res, next) => this.authCredentialsController.register(req, res, next));
    }
};
Api = tslib_1.__decorate([
    typedi_1.Service(),
    tslib_1.__metadata("design:paramtypes", [company_controller_1.CompanyController,
        company_type_controller_1.CompanyTypeController,
        auth_credentials_controller_1.AuthCredentialsController])
], Api);
exports.Api = Api;
//# sourceMappingURL=api.js.map