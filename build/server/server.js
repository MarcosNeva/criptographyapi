"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Server = void 0;
const tslib_1 = require("tslib");
const body_parser_1 = require("body-parser");
const typedi_1 = require("typedi");
const cors = require("cors");
const express = require("express");
const morgan = require("morgan");
const api_1 = require("./api/api");
const environment_1 = require("../config/environment");
const jwt_auth_middleware_1 = require("../auth/auth-middleware/jwt.auth-middleware");
let Server = class Server {
    constructor(api, jwtMiddleware) {
        this.api = api;
        this.jwtMiddleware = jwtMiddleware;
        this.app = express();
        this.setupServer();
    }
    setupServer() {
        this.app.use(cors());
        this.app.use(body_parser_1.json({ limit: '5mb' }));
        this.app.use(body_parser_1.urlencoded({ extended: false }));
        this.app.use(morgan('dev'));
        this.app.use('/auth', this.api.getAuthRouter()); //Router diferente para que el jwtMiddleware no afecte a todos los endpoints de /auth. Si no, necesitaríamos un token para realizar un login y sería imposible
        this.app.use('/api', this.jwtMiddleware.validateRequest); //Comprobar el token que nos permitirá firmar las consultas
        this.app.use('/api', this.api.getApiRouter()); //Realizar la consulta pedida
        this.httpServer = this.app.listen(environment_1.config.port, this.onHttpServerListening);
    }
    onHttpServerListening() {
        console.log('Server Express started in %s mode (ip: %s, port: %s)', environment_1.config.env, environment_1.config.ip, environment_1.config.port);
    }
};
Server = tslib_1.__decorate([
    typedi_1.Service(),
    tslib_1.__metadata("design:paramtypes", [api_1.Api, jwt_auth_middleware_1.JwtMiddleware])
], Server);
exports.Server = Server;
//# sourceMappingURL=server.js.map