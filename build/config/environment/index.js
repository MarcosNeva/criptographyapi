"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = void 0;
const lodash_1 = require("lodash");
const development_1 = require("./development");
const test_1 = require("./test");
const all = {
    env: process.env.NODE_ENV,
    port: process.env.PORT ? Number(process.env.PORT) : 8080,
    ip: process.env.IP || '0.0.0.0',
    user_forgot_pass_key: '3ac1194d22d53db7e2425d8f',
    user_sessions: {
        // Token secreto para la encriptación de los JWT
        secret: 'rgdj22qh323gfydda.1ej,pg3dfa.hrjf489dh24a.435.23.scaffolding',
        // Número de días a los que expirará la sesión
        expiration_days: 7,
        // Número máximo de sesiones activas concurrentemente
        max_active_sessions: 4
    }
};
exports.config = lodash_1.merge(all, _getEnvironmentConfig());
function _getEnvironmentConfig() {
    if (process.env.NODE_ENV === 'development') {
        return development_1.development;
    }
    else if (process.env.NODE_ENV === 'test') {
        return test_1.test;
    }
    else {
        return {};
    }
}
//# sourceMappingURL=index.js.map