"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyTypeController = void 0;
const tslib_1 = require("tslib");
const typedi_1 = require("typedi");
const company_type_service_1 = require("./company-type.service");
let CompanyTypeController = class CompanyTypeController {
    constructor(companyTypeService) {
        this.companyTypeService = companyTypeService;
    }
    /**
     * @api GET /companyTypes
     *
     * @param req
     * @param res
     * @param next
     */
    getAll(req, res, next) {
        this.companyTypeService.findAll()
            .then((companyTypeList) => {
            res.send(companyTypeList);
        });
    }
};
CompanyTypeController = tslib_1.__decorate([
    typedi_1.Service(),
    tslib_1.__metadata("design:paramtypes", [company_type_service_1.CompanyTypeService])
], CompanyTypeController);
exports.CompanyTypeController = CompanyTypeController;
//# sourceMappingURL=company-type.controller.js.map