"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyTypeService = void 0;
const tslib_1 = require("tslib");
const typedi_1 = require("typedi");
const company_type_repository_1 = require("./repository/company-type.repository");
let CompanyTypeService = class CompanyTypeService {
    constructor(companyTypeRepository) {
        this.companyTypeRepository = companyTypeRepository;
    }
    findAll() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield this.companyTypeRepository.findAll();
        });
    }
};
CompanyTypeService = tslib_1.__decorate([
    typedi_1.Service(),
    tslib_1.__metadata("design:paramtypes", [company_type_repository_1.CompanyTypeRepository])
], CompanyTypeService);
exports.CompanyTypeService = CompanyTypeService;
//# sourceMappingURL=company-type.service.js.map