"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyTypeRepository = void 0;
const tslib_1 = require("tslib");
const database_1 = require("../../../database/database");
const typedi_1 = require("typedi");
let CompanyTypeRepository = class CompanyTypeRepository {
    constructor(databaseService) {
        this.databaseService = databaseService;
    }
    findAll() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const queryDoc = {
                sql: 'SELECT * FROM core.company_types'
            };
            const companyTypes = yield this.databaseService.execQuery(queryDoc);
            return companyTypes.rows;
        });
    }
};
CompanyTypeRepository = tslib_1.__decorate([
    typedi_1.Service(),
    tslib_1.__metadata("design:paramtypes", [database_1.DatabaseService])
], CompanyTypeRepository);
exports.CompanyTypeRepository = CompanyTypeRepository;
//# sourceMappingURL=company-type.repository.js.map