"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyController = void 0;
const tslib_1 = require("tslib");
const typedi_1 = require("typedi");
const company_service_1 = require("./company.service");
let CompanyController = class CompanyController {
    constructor(companyService) {
        this.companyService = companyService;
    }
    /**
     * @api POST /companies
     *
     * This method creates a new company
     *
     * @param req
     * @param res
     * @param next
     */
    create(req, res, next) {
        if (req.body) {
            const company = req.body;
            this.companyService.create(company)
                .then((newCompany) => {
                res.send(newCompany);
            })
                .catch((error) => {
                res.sendStatus(500);
            });
        }
    }
    /**
     * @api GET /companies
     *
     * @param req
     * @param res
     * @param next
     */
    getAll(req, res, next) {
        this.companyService.findAll()
            .then((companyList) => {
            res.send(companyList);
        })
            .catch((error) => {
            res.sendStatus(500);
        });
    }
    /**
      * @api PUT /companies/:id
      *
      * @param req
      * @param res
      * @param next
      */
    update(req, res, next) {
        if (req.body && req.params.id) {
            const company = req.body;
            company.id = req.params.id;
            this.companyService.update(company)
                .then((updatedCompany) => {
                res.send(updatedCompany);
            })
                .catch((error) => {
                res.sendStatus(500);
            });
        }
    }
    /**
     * @api GET /companies/:id
     *
     * @param req
     * @param res
     * @param next
     */
    getById(req, res, next) {
        if (req.params.id) {
            this.companyService.findById(req.params.id)
                .then((company) => {
                res.send(company);
            })
                .catch((error) => {
                res.sendStatus(500);
            });
        }
    }
    /**
     *
     * @api DELETE /companies/:id
     *
     * @param req
     * @param res
     * @param next
     */
    delete(req, res, next) {
        if (req.params.id) {
            this.companyService.remove(req.params.id)
                .then(() => {
                res.sendStatus(200);
            })
                .catch((error) => {
                res.sendStatus(500);
            });
        }
    }
};
CompanyController = tslib_1.__decorate([
    typedi_1.Service(),
    tslib_1.__metadata("design:paramtypes", [company_service_1.CompanyService])
], CompanyController);
exports.CompanyController = CompanyController;
//# sourceMappingURL=company.controller.js.map