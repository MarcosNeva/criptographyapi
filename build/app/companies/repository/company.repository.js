"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyRepository = void 0;
const tslib_1 = require("tslib");
const database_1 = require("../../../database/database");
const typedi_1 = require("typedi");
const search_filter_service_1 = require("../../common/search-filter.service");
let CompanyRepository = class CompanyRepository {
    constructor(databaseService, searchFilterService) {
        this.databaseService = databaseService;
        this.searchFilterService = searchFilterService;
    }
    create({ company_typeId, name, address = null, phone = null, cif = null, admin = false }) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const queryDoc = {
                sql: 'INSERT INTO core.companies ("company_typeId", name, address, phone, cif, admin) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *',
                params: [company_typeId, name, address, phone, cif, admin]
            };
            const company = yield this.databaseService.execQuery(queryDoc);
            return company.rows[0];
        });
    }
    update(companyId, company) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const { name, address, phone, cif } = company;
            const queryDoc = {
                sql: 'UPDATE core.companies SET name = $1, address = $2, phone = $3, cif = $4 WHERE id = $5 RETURNING *',
                params: [name, address, phone, cif, companyId]
            };
            const updatedCompany = yield this.databaseService.execQuery(queryDoc);
            return updatedCompany.rows[0];
        });
    }
    setActivationFlag(companyId, isActive) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const queryDoc = {
                sql: 'UPDATE core.companies SET active = $1 WHERE id = $2 RETURNING *',
                params: [isActive, companyId]
            };
            const updatedCompany = yield this.databaseService.execQuery(queryDoc);
            return updatedCompany.rows[0];
        });
    }
    findAll(searchFilter) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const queryDoc = {
                sql: 'SELECT * FROM core.companies'
            };
            if (searchFilter != null) {
                const whereDoc = this.searchFilterService.transformSearchFilterToSQL(searchFilter);
                if (searchFilter.search != null || searchFilter.filter != null) {
                    queryDoc.sql = queryDoc.sql + ` WHERE `;
                }
                queryDoc.sql = queryDoc.sql + whereDoc.sql;
                queryDoc.params = whereDoc.params;
            }
            const companies = yield this.databaseService.execQuery(queryDoc);
            return companies.rows;
        });
    }
    findById(companyId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const queryDoc = {
                sql: 'SELECT * FROM core.companies WHERE id = $1',
                params: [companyId]
            };
            const companies = yield this.databaseService.execQuery(queryDoc);
            return companies.rows[0] || null;
        });
    }
    remove(companyId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const queryDoc = {
                sql: 'DELETE FROM core.companies WHERE id = $1 RETURNING *',
                params: [companyId]
            };
            const updatedCompany = yield this.databaseService.execQuery(queryDoc);
            return updatedCompany.rows[0] || null;
        });
    }
};
CompanyRepository = tslib_1.__decorate([
    typedi_1.Service(),
    tslib_1.__metadata("design:paramtypes", [database_1.DatabaseService,
        search_filter_service_1.SearchFilterService])
], CompanyRepository);
exports.CompanyRepository = CompanyRepository;
//# sourceMappingURL=company.repository.js.map