"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyService = void 0;
const tslib_1 = require("tslib");
const typedi_1 = require("typedi");
const lodash_1 = require("lodash");
const company_repository_1 = require("./repository/company.repository");
let CompanyService = class CompanyService {
    constructor(companyRepository) {
        this.companyRepository = companyRepository;
    }
    create(company) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!this.isValidCompany(company)) {
                return Promise.reject(new Error('CompanyInputValidationError'));
            }
            return yield this.companyRepository.create(company);
        });
    }
    update(company) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!this.isValidId(company.id)) {
                return Promise.reject(new Error('InvalidCompanyIdError'));
            }
            if (!this.isValidCompany(company)) {
                return Promise.reject(new Error('CompanyInputValidationError'));
            }
            return yield this.companyRepository.update(company.id, company);
        });
    }
    activate(companyId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!this.isValidId(companyId)) {
                return Promise.reject(new Error('InvalidCompanyIdError'));
            }
            return yield this.companyRepository.setActivationFlag(companyId, true);
        });
    }
    deactivate(companyId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!this.isValidId(companyId)) {
                return Promise.reject(new Error('InvalidCompanyIdError'));
            }
            return yield this.companyRepository.setActivationFlag(companyId, false);
        });
    }
    findAll(searchFilter) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield this.companyRepository.findAll(searchFilter);
        });
    }
    findById(companyId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!this.isValidId(companyId)) {
                return Promise.reject(new Error('InvalidCompanyIdError'));
            }
            return yield this.companyRepository.findById(companyId);
        });
    }
    remove(companyId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!this.isValidId(companyId)) {
                return Promise.reject(new Error('InvalidCompanyIdError'));
            }
            return yield this.companyRepository.remove(companyId);
        });
    }
    isValidId(companyId) {
        return companyId != null && lodash_1.isNumber(lodash_1.toNumber(companyId)) && lodash_1.toNumber(companyId) > 0;
    }
    isValidCompany(company) {
        return company != null
            && company.name != null && lodash_1.isString(company.name)
            && company.company_typeId != null && lodash_1.isNumber(lodash_1.toNumber(company.company_typeId));
    }
};
CompanyService = tslib_1.__decorate([
    typedi_1.Service(),
    tslib_1.__metadata("design:paramtypes", [company_repository_1.CompanyRepository])
], CompanyService);
exports.CompanyService = CompanyService;
//# sourceMappingURL=company.service.js.map