"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const tslib_1 = require("tslib");
const database_1 = require("../../../database/database");
const typedi_1 = require("typedi");
const search_filter_service_1 = require("../../common/search-filter.service");
let UserRepository = class UserRepository {
    constructor(databaseService, searchFilterService) {
        this.databaseService = databaseService;
        this.searchFilterService = searchFilterService;
    }
    //Repositorio similar a companies, pero ahora necesitamos usuarios en la BBDD, necesitamos el método create, findAll, findByName y findUser
    // Permite crear usuarios, se insertan con sql en la tabla user los datos proporcionados
    create(user_Id, name, surname, email, password) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const queryDoc = {
                sql: 'INSERT INTO core.user (id, name, surname, email, password) VALUES ($1, $2, $3, $4, $5) RETURNING *',
                params: [user_Id, name, surname, email, password]
            };
            const user = yield this.databaseService.execQuery(queryDoc);
            return user.rows[0];
        });
    }
    // Se genera una lista de usuarios, incluyendo todos los campos que tienen
    findAll(searchFilter) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const queryDoc = {
                sql: 'SELECT * FROM core.user'
            };
            if (searchFilter != null) {
                const whereDoc = this.searchFilterService.transformSearchFilterToSQL(searchFilter);
                if (searchFilter.search != null || searchFilter.filter != null) {
                    queryDoc.sql = queryDoc.sql + ` WHERE `;
                }
                queryDoc.sql = queryDoc.sql + whereDoc.sql;
                queryDoc.params = whereDoc.params;
            }
            const users = yield this.databaseService.execQuery(queryDoc);
            return users.rows;
        });
    }
    //Nos permite identificar un usuario con un nombre específico
    findByName(name) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const queryDoc = {
                sql: 'SELECT * FROM core.user WHERE name = $1',
                params: [name]
            };
            const users = yield this.databaseService.execQuery(queryDoc);
            return users.rows[0] || null;
        });
    }
    //Nos permite identificar un usuario con un mail y contraseña específico
    findUser(email, password) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const queryDoc = {
                sql: 'SELECT * FROM core.user WHERE email = $1 AND password = $2',
                params: [email, password]
            };
            const users = yield this.databaseService.execQuery(queryDoc);
            return users.rows[0] || null;
        });
    }
};
UserRepository = tslib_1.__decorate([
    typedi_1.Service(),
    tslib_1.__metadata("design:paramtypes", [database_1.DatabaseService,
        search_filter_service_1.SearchFilterService])
], UserRepository);
exports.UserRepository = UserRepository;
//# sourceMappingURL=user.repository.js.map